# API Client

With this API Client library and your `pokamax.com` API credentials you can order greeting cards.

There are two ways to order. A simple way using a picture and a greeting text, and one that allows you to provide your greeting card as a PDF. See "Usage" below for more instructions how to use this library.

This library supports PHP versions 7.2 - 8.2.7 and uses the guzzle library version 7.7.

## Installing dependencies

`composer install`

# Usage

#### How to import and use this library in your project

This is an example of how to create a new project from scratch and use it. If you already have a project you can skip some of these steps of course.

###### Project setup

```bash
mkdir project
cd project
composer init
# Composer will ask you some details about the project
# When it asks about the project type, you probably want to select "project"
```

Now add this to your composer.json:

```json
{
...
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/pokamax/reseller-api-client-php"
        }
    ],
...
    "require": {
        "pokamax/pokamax-client": "dev-main"
    }
}
```

Then run `composer install` to install the library.

Now you can copy the `example.php` into your project from the example repo, rename it and customize it.

###### How to get your API credentials

After signing up at pokamax.com, apply to the API using one of the following links:

- https://pokamax.com/de/partner/api
- https://pokamax.com/us/partner/api

After applying by filling out a form you will receive the credentials and can start using the API in test mode. That means you can test the API, but no real cards will be sent yet.

The support staff will then reach out to you with a contract including individual pricing for a monthly subscription. After signing the contract, the API gets activated and you can start sending cards.

###### Running

Then you can run it, e.g. `php example.php`.


#### Initializing the Client

Import the library and configure the Client as below. Depending on your project setup, you will probably have to change the first line as needed so PHP can import the library.

```php
require_once __DIR__ . "/vendor/autoload.php";

use Pokamax\{Client, Address, Postcard, GreetingCard};

// Configure the client with your reseller account email and API token
$client = new Client([
    "email" => "your reseller account email address",
    "token" => "your API token",
    "productionMode" => false
]);
```

#### Simple order, using a picture

The value of "format" and the picture dimensions can be any one of the following supported formats:

| Constant                          | Optimal picture dimensions |
| --------------------------------- | -------------------------- |
| Postcard::StandardLandscape       | 1819x1311                  |
| Postcard::JumboLandscape          | 2787x1488                  |
| GreetingCard::StandardLandscapeFT | 1819x1275                  |
| GreetingCard::StandardPortraitFL  | 1275x1819                  |

```php
$address = new Address([
    "name" => "Franz Test",
    "street" => "Musterstr. 2",
    "zip" => "22345",
    "city" => "Hamburg",
    "country" => "DE"
]);

// The path to your greeting card picture
$picturePath = __DIR__ . "/tests/files/picture.jpg";
$client->orderCard([
    "format" => Postcard::StandardLandscape,
    "picture" => $picturePath,
    "greetingText" => "Happy birthday!",
    "address" => $address
]);
```

#### PDF order

```php
// The path to your pdf
$pdfPath = __DIR__ . "/tests/files/prerendered.pdf";
$client->orderPdfCard([
    // Other possible formats are possible, as in the simple order
    "format" => Postcard::StandardLandscape,
    "pdf" => $pdfPath,
    "address" => $address // Same as with the simple order above
]);
```

The PDF should conform to the following specifications:

| Format                      | Spec                                                         |
| --------------------------- | ------------------------------------------------------------ |
| Postcard::StandardLandscape | 300ppi/dpi, 437x315 pts, 2 pages, bleeding box (metadata in the pdf) is being utilized should be set to the edges of the pages. |



------

# Development

## PHP version
To use a specific PHP version, you can use phpbrew. The version used for development is 7.2, and the
project is also tested on 8.2.7.

## Installing dependencies
`composer install`

## Testing

1. pokamax-backend needs to be running at localhost:3000
2. Add api credentials in the TestCase's `setUp()`-functions
3. Run tests: `./vendor/bin/phpunit`
4. To easily run tests with newer PHP Versions than otherwise supported, after installing that PHP version with phpbrew: `/home/user/.phpbrew/php/php-8.2.7/bin/php vendor/bin/phpunit`
