<?php
    namespace Pokamax;

    use GuzzleHttp\RequestOptions;

    class POSTFile {
        private $paramName;
        private $path;

        public function __construct($paramName, $path) {
            $this->paramName = $paramName;
            $this->path = $path;
        }

        public function toMultipart() {
            return [
                'name' => $this->paramName,
                'contents' => fopen($this->path, 'r')
            ];
        }
    }

    class Client {
        const VERSION = "1.0";

        private $authToken = null;
        private $authEmail = null;
        private $apiURL = null;
        private $guzzleClient = null;
        private $productionMode = true;

        private $pictureCache = array();

        public function __construct($options) {
            $this->authEmail = $options["email"];
            $this->authToken = $options["token"];
            $this->productionMode = $options['productionMode'] ?? true;
            $this->apiURL = $options["apiURL"] ?? "https://pokamax.com/apis/reseller/v1";
            $this->guzzleClient = $options["guzzleClient"] ?? new \GuzzleHttp\Client();
        }

        private function endpointURL($endpoint) {
            return $this->apiURL . "/$endpoint";
        }

        private function sendPostWithFileJSON($endpoint, $file) {
            $response = self::sendPostWithFiles($endpoint, [$file]);

            $body = $response->getBody()->getContents();
            $json = json_decode($body, true);

            return $json;
        }

        private function sendPostWithFiles($endpoint, $files, $requestOptions = []) {
            $headers = [
                'Authorization' => 'Token token="' . $this->authToken . '", email="' . $this->authEmail . '"',
                'User-Agent' => 'pokamax-client v' . self::VERSION . '; PHP',
            ];

            $filesMultipart = array_map(function ($file) { return $file->toMultipart(); }, $files);
            $optionsMultipart = array_map(function ($key, $value) {
                return [
                    'name' => $key,
                    'contents' => $value
                ];
            }, array_keys($requestOptions), $requestOptions);

            $options = [
                RequestOptions::HEADERS => $headers,
                RequestOptions::MULTIPART => array_merge($filesMultipart, $optionsMultipart)
            ];

            $response = $this->guzzleClient->post($this->endpointURL($endpoint), $options);
            return $response;
        }

        public function orderCard($opts) {
            $order = new Order();
            $order->addSimpleCard($opts["format"], $opts["picture"], $opts["greetingText"], $opts["address"]);
            $this->sendOrder($order);
        }

        public function orderPdfCard($opts) {
            $order = new Order();
            $order->addPdfCard($opts["format"], $opts["pdf"], $opts["address"]);
            $this->sendOrder($order);
        }

        public function sendOrder($order) {
            $cards = $order->getCards();
            $files = array();

            $requestOptions = array(
                'test' => $this->productionMode ? '0' : '1'
            );

            foreach ($cards as $index => $card) {
                if (is_a($card, 'Pokamax\SimpleCard')) {
                    if (!$card->picture->isPersisted()) {
                        $card->picture = $this->uploadPicture($card->picture);
                    }

                    $requestOptions['order[cards_attributes][' . $index . '][product_name]'] = $card->format;
                    $requestOptions['order[cards_attributes][' . $index . '][picture_id]'] = $card->picture->id;
                    $requestOptions['order[cards_attributes][' . $index . '][text]'] = $card->greeting_text;
                    $requestOptions['order[cards_attributes][' . $index . '][company]'] = $card->address->company;
                    $requestOptions['order[cards_attributes][' . $index . '][title]'] = $card->address->title;
                    $requestOptions['order[cards_attributes][' . $index . '][name]'] = $card->address->name;
                    $requestOptions['order[cards_attributes][' . $index . '][street]'] = $card->address->street;
                    $requestOptions['order[cards_attributes][' . $index . '][zip]'] = $card->address->zip;
                    $requestOptions['order[cards_attributes][' . $index . '][city]'] = $card->address->city;
                    $requestOptions['order[cards_attributes][' . $index . '][state]'] = $card->address->state;
                    $requestOptions['order[cards_attributes][' . $index . '][country]'] = $card->address->country;
                } elseif (is_a($card, 'Pokamax\PdfCard')) {
                    $files[] = new POSTFile('order[cards_attributes][' . $index . '][pdf]', $card->pdf);

                    $requestOptions['order[cards_attributes][' . $index . '][product_name]'] = $card->format;
                    $requestOptions['order[cards_attributes][' . $index . '][company]'] = $card->address->company;
                    $requestOptions['order[cards_attributes][' . $index . '][title]'] = $card->address->title;
                    $requestOptions['order[cards_attributes][' . $index . '][name]'] = $card->address->name;
                    $requestOptions['order[cards_attributes][' . $index . '][street]'] = $card->address->street;
                    $requestOptions['order[cards_attributes][' . $index . '][zip]'] = $card->address->zip;
                    $requestOptions['order[cards_attributes][' . $index . '][city]'] = $card->address->city;
                    $requestOptions['order[cards_attributes][' . $index . '][state]'] = $card->address->state;
                    $requestOptions['order[cards_attributes][' . $index . '][country]'] = $card->address->country;
                }
            }

            $response = self::sendPostWithFiles("orders", $files, $requestOptions);
        }

        public function uploadPicture($picture) {
            $file = $picture->getLocalPicture();
            if (array_key_exists($file, $this->pictureCache)) {
                $cachedPic = $this->pictureCache[$file];
                $picture->onUpload($cachedPic);
                return $cachedPic;
            }

            if (!is_null($picture->id)) {
                throw new \Exception("Picture already saved!");
            }

            $post_file = new POSTFile("picture[file]", $file);

            $json = self::sendPostWithFileJSON('pictures', $post_file);

            $newPicture = new Picture();
            $newPicture->id = intval($json['id']);
            $newPicture->thumbUrl = $json['thumb_url'];
            $newPicture->createdAt = $json['created_at'];

            $picture->onUpload($newPicture);

            $this->pictureCache[$file] = $picture;

            $picture->id = intval($json['id']);

            return $picture;
        }
    }
