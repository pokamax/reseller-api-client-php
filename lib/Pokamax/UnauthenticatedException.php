<?php

    namespace Pokamax;

    class UnauthenticatedException extends \Exception {
        public function __construct() {
            parent::__construct('Unauthenticated', 401);
        }

        public function __toString() {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }