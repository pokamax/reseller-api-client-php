<?php
namespace Pokamax;

use Symfony\Component\Intl\Countries;

    class Address {
        public $company = null;
        public $title = null;
        public $name = null;
        public $street = null;
        public $zip = null;
        public $city = null;
        public $state = null;
        public $country = null;

        public function __construct($options) {
            $this->name = $options["name"];
            $this->street = $options["street"];
            $this->zip = $options["zip"];
            $this->city = $options["city"];
            $this->country = $this->validateCountry($options["country"]);
            $this->title = $options["title"] ?? null;
            $this->state = $options["state"] ?? null;
            $this->company = $options["company"] ?? null;
        }

        private function validateCountry($countryCode) {
            if (!Countries::exists($countryCode)) {
                throw new \Exception("Not a valid ISO 3166-1 country code: " . $countryCode);
            } else {
                return $countryCode;
            }
        }
    }
