<?php

namespace Pokamax;

/**
 * The PDF specs formats other than Postcard::StandardLandscape are currently unknown
 */
class GreetingCard {
    /**
     * GreetingCard Standard Landscape Fold Top
     * Optimal picture dimensions: 1819x1275
     */
    const StandardLandscapeFT = "Standard_QUER_O";

    /**
     * GreetingCard Standard Portrait Fold Left
     * Optimal picture dimensions: 1275x1819
     */
    const StandardPortraitFL = "Standard_HOCH_L";
}
