<?php

namespace Pokamax;

class Postcard {
   /**
    * PDF spec for Standard Postcard:
    * 300ppi/dpi
    * 437 x 315 pts
    * 2 pages
    * bleeding box (metadata in the pdf) is being utilized and should be set to the edges of the pages.
    *
    * The PDF specs for other formats are currently unknown
    *
    *
    * Postcard Standard Landscape
    * Optimal picture dimensions: 1819x1311
    */
    const StandardLandscape = "standard";

    /**
    * Postcard Jumbo Landscape
    * Optimal picture dimensions: 2787x1488
    */
    const JumboLandscape = "jumbo";
}
