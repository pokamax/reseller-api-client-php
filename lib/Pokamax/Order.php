<?php

    namespace Pokamax;

    class Order {

        private $cards = array();

        public function addPdfCard($format, $pdf, $address) {
            $card = new PdfCard();
            $card->format = $format;
            $card->pdf = $pdf;
            $card->address = $address;
            $this->cards[] = $card;
        }

        public function addSimpleCard($format, $picture, $greeting_text, $address) {
            $card = new SimpleCard();
            $card->format = $format;
            if (is_int($picture)) {
                $pic = new Picture();
                $pic->id = $picture;
                $card->picture = $pic;
            } elseif (is_string($picture)) {
                $pic = new Picture($picture);
                $card->picture = $pic;
            } elseif (is_a($picture, "Pokamax\Picture")) {
                $card->picture = $picture;
            } else {
                throw new \InvalidArgumentException("Picture unknown format");
            }

            $card->greeting_text = $greeting_text;
            $card->address = $address;
            $this->cards[] = $card;
        }

        public function cardsCount() {
            return count($this->cards);
        }

        public function getCards() {
            return $this->cards;
        }
    }
