<?php

    namespace Pokamax;

    class InvalidDataException extends \Exception {
        public function __construct() {
            parent::__construct('Invalid Data', 422);
        }

        public function __toString() {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }