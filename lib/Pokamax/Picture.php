<?php

    namespace Pokamax;

    class Picture {
        public $id = null;
        public $thumbUrl = null;
        public $createdAt = null;
        public $priceCents = 0;
        public $locale = null;
        public $text = null;

        private $localPicture = null;

        public function __construct($path = null) {
            if (!is_null($path)) {
                $this->setLocalPicture($path);
            }
        }

        public static function getById($id) {
            $pic = new Picture();
            $pic->id = $id;
            $pic->thumbUrl = "";
            $pic->createdAt = "";
            return $pic;
        }

        public function getLocalPicture() {
            return $this->localPicture;
        }

        public function setLocalPicture($path) {
            if (!is_null($this->id)) {
                throw new \Exception("An already saved Picture can't be changed any more.");
            }

            if (is_file($path) && is_readable($path)) {
                $this->localPicture = $path;
            } else {
                throw new \Exception("Picture '{$path}' does not exist or is not readable");
            }
        }

        public function getPrice() {
            return $this->priceCents;
        }

        public function isPersisted() {
            return !is_null($this->id);
        }

        public function onUpload($response) {
            $this->id = $response->id;
            $this->thumbUrl = $response->thumbUrl;
            $this->createdAt = $response->createdAt;
            $this->priceCents = 0;
        }
    }
