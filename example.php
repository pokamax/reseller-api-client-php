<?php

require_once __DIR__ . "/vendor/autoload.php";

use Pokamax\{Client, Address, Postcard, GreetingCard};

// Configure the client with your reseller account email and API token
$client = new Client([
    "email" => "your reseller account email address",
    "token" => "your API token",
    // This is for testing, if this is set to false no actual orders will be sent.
    // Remove this line or set to true for actual production usage
    "productionMode" => false
]);

// Address of the recipient of the card
$address = new Address([
    "name" => "Franz Test",
    "street" => "Musterstr. 2",
    "zip" => "22345",
    "city" => "Hamburg",
    "country" => "DE"
]);

// Send a simple order, using a picture and a greeting text
//
// The path to your greeting card picture
$picturePath = __DIR__ . "/tests/files/picture.jpg";
$client->orderCard([
    // Other possible formats:
    // Postcard::JumboLandscape
    // GreetingCard::StandardLandscapeFT
    // GreetingCard::StandardPortraitFL
    "format" => Postcard::StandardLandscape,
    "picture" => $picturePath,
    "greetingText" => "Happy birthday!",
    "address" => $address
]);

// Send a order using a PDF
//
// The path to your PDF
$pdfPath = __DIR__ . "/tests/files/prerendered.pdf";
$client->orderPdfCard([
    // Other possible formats are possible, as in the simple order
    "format" => Postcard::StandardLandscape,
    "pdf" => $pdfPath,
    "address" => $address
]);
