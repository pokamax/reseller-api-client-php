<?php

namespace Test;

use GuzzleMockHandler\GuzzleMockHandler;
use GuzzleMockHandler\GuzzleMockResponse;
use Pokamax\Client;

define('PROJECT_DIR', dirname(__FILE__) . "/../.." );

class TestHelper {
    public static $picturePath = PROJECT_DIR . '/tests/files/picture.jpg';

    public static $clientDefaults = [
            "email" => "reseller@example.com",
            "token" => "some-random-token",
            "productionMode" => false,
            "apiURL" => "http://localhost:3000/apis/reseller/v1"
    ];

    public static $pictureUploadResponse = [
        "id" => 351,
        "thumb_url" => "http://localhost:3000/media/W1siZiIsInBpY3R1cmVzLzIwMjNfMDcvMjAxNDlkMGEtMjViZS00YTJjLWExYWYtZWI4N2FkNjU3ZjZlLmpwZyJdLFsicCIsInRodW1iIiwiMjAweDIwMCJdXQ?sha=9a0c8326f6257665",
        "created_at" => "2023-07-06T15:15:42.303+02:00"
    ];

    public static function clientWithMockedPictureUploadResponse() {
        $expectedRequest = [
            'picture[file]' => file_get_contents(self::$picturePath)
        ];
        return self::clientWithMockResponse(
            (new GuzzleMockResponse("/apis/reseller/v1/pictures"))
                ->withMethod("post")
                ->withBody(self::$pictureUploadResponse)
                ->assertMultipartRequest($expectedRequest)
        );
    }

    public static function clientWithMockResponses($mockResponses) {
        $guzzleClient = self::guzzleWithMockResponses($mockResponses);
        $overrides = ["guzzleClient" => $guzzleClient];
        return new Client(array_merge(self::$clientDefaults, $overrides));
    }

    public static function clientWithMockResponse($mockResponse) {
        return self::clientWithMockResponses([$mockResponse]);
    }

    public static function guzzleWithMockResponses($mockResponses) {
        $handler = new GuzzleMockHandler;

        foreach($mockResponses as $mockResponse) {
            $handler->expect($mockResponse);
        }

        $stack = \GuzzleHttp\HandlerStack::create($handler);
        return new \GuzzleHttp\Client(['handler' => $stack]);
    }

    public static function guzzleWithMockResponse($mockResponse) {
        return self::guzzleWithMockResponses([$mockResponse]);
    }
}
