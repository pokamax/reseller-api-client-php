<?php

use PHPUnit\Framework\TestCase as TestCase;
use Pokamax\{Client, Address, Postcard};
use Test\TestHelper;

class AddressTest extends TestCase {
    public function testCountryCodeValidation() {
        new Address([
            "name" => "Frany Test",
            "street" => "Musterstr. 2",
            "zip" => "22345",
            "city" => "Hamburg",
            "country" => "DE"
        ]);

        $this->expectException('\Exception');
        new Address([
            "name" => "Frany Test",
            "street" => "Musterstr. 2",
            "zip" => "22345",
            "city" => "Hamburg",
            "country" => "INVALID"
        ]);
    }
}
