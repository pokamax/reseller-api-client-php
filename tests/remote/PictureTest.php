<?php

use PHPUnit\Framework\TestCase as TestCase;
use Pokamax\Picture;
use Pokamax\Client;
use GuzzleMockHandler\GuzzleMockResponse;
use Test\TestHelper;

class PictureTest extends TestCase {

    public function testSetUnavailableLocalPictureFails() {
        $this->expectException('Exception');
        $pic = new Picture("/does/not/exist");
    }

    public function testBuildPictureFromLocalFile() {
        $pic = new Picture(TestHelper::$picturePath);
        $this->assertEquals(TestHelper::$picturePath, $pic->getLocalPicture());
        $this->assertNull($pic->id);
        $this->assertNull($pic->thumbUrl);
        $this->assertNull($pic->createdAt);
        $this->assertEquals(0, $pic->getPrice());
    }

    public function testUploadLocalPicture() {
        $pic = new Picture(TestHelper::$picturePath);
        $client = TestHelper::clientWithMockedPictureUploadResponse();
        $client->uploadPicture($pic);
        $this->assertNotNull($pic->id);
        $this->assertNotNull($pic->thumbUrl);
        $this->assertNotNull($pic->createdAt);
        $this->assertEquals(0, $pic->getPrice());
    }

    public function testReuploadPicture() {
        $picture = new Picture(TestHelper::$picturePath);
        $client = TestHelper::clientWithMockedPictureUploadResponse();
        $r1 = $client->uploadPicture($picture);
        $r2 = $client->uploadPicture($picture);

        $this->assertEquals($r1->id, $r2->id);
    }

    public function testChangeLocalPictureOnSavedPictureFails() {
        $expectedRequest = [
            'picture[file]' => file_get_contents(TestHelper::$picturePath)
        ];
        $pic = new Picture(TestHelper::$picturePath);
        $client = TestHelper::clientWithMockResponse(
            (new GuzzleMockResponse("/apis/reseller/v1/pictures"))
                ->withMethod("post")
                ->withBody(TestHelper::$pictureUploadResponse)
                ->assertMultipartRequest($expectedRequest)
        );
        $client->uploadPicture($pic);

        $this->expectException('Exception');
        $pic->setLocalPicture(TestHelper::$picturePath);
    }

    public function testGetPictureByID() {
        $id = 123;

        $pic = Picture::getById($id);
        $this->assertInstanceOf('Pokamax\Picture', $pic);
        $this->assertEquals($id, $pic->id);
        $this->assertNotNull($pic->thumbUrl);
        $this->assertNotNull($pic->createdAt);
        #todo: implement it
        #$this->assertEquals(0, $pic->getPrice());
    }

    #todo: implement it!
    #public function testGetUnknownPictureByIDFails()
    #{
    #    $id= 123456789;
    #
    #    $this->expectException('Exception');
    #    $pic = Picture::getById($id);
    #}
}
