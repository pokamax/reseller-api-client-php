<?php

use PHPUnit\Framework\TestCase as TestCase;
use Pokamax\{Client, Address, Postcard};
use GuzzleMockHandler\GuzzleMockResponse;
use Test\TestHelper;

class ClientTest extends TestCase {
    private $client;

    public function testUnauthenticatedException() {
        $this->expectException('GuzzleHttp\Exception\ClientException');

        $expectedRequest = [
            'picture[file]' => file_get_contents(TestHelper::$picturePath)
        ];

        // The call to orders will never be made because the call to pictures comes first and is
        // answered with 401
        $guzzleClient = TestHelper::guzzleWithMockResponse(
            (new GuzzleMockResponse("/apis/reseller/v1/pictures"))
                ->withMethod("post")
                ->withStatus(401)
                ->assertMultipartRequest($expectedRequest)
        );

        $client = new Client([
            "email" => "",
            "token" => "",
            "productionMode" => false,
            "apiURL" => "http://localhost:3000/apis/reseller/v1",
            "guzzleClient" => $guzzleClient
        ]);

        $client->orderCard([
            "format" => Postcard::StandardLandscape,
            "picture" => TestHelper::$picturePath,
            "greetingText" => "text",
            "address" => $this->getValidAddress()
        ]);
    }

    public function testProductionModeConfig() {
        $this->expectNotToPerformAssertions();
        $client = new Client([
            "email" => "",
            "token" => "",
            "apiURL" => "http://localhost:3000/apis/reseller/v1"
        ]);
    }

    public function testOrderCardWithID() {
        $expectedRequest = [
                'test' => '1',
                'order[cards_attributes][0][product_name]' => 'standard',
                'order[cards_attributes][0][picture_id]' => '9',
                'order[cards_attributes][0][text]' => 'text',
                'order[cards_attributes][0][company]' => '',
                'order[cards_attributes][0][title]' => '',
                'order[cards_attributes][0][name]' => 'Frany Test',
                'order[cards_attributes][0][street]' => 'Musterstr. 2',
                'order[cards_attributes][0][zip]' => '22345',
                'order[cards_attributes][0][city]' => 'Hamburg',
                'order[cards_attributes][0][state]' => '',
                'order[cards_attributes][0][country]' => 'DE'
        ];
        $client = TestHelper::clientWithMockResponse(
            (new GuzzleMockResponse("/apis/reseller/v1/orders"))
                ->withMethod("post")
                ->assertMultipartRequest($expectedRequest)
        );
        # Picture with id 9 has to exist in the backend
        $client->orderCard([
            "format" => Postcard::StandardLandscape,
            "picture" => 9,
            "greetingText" => "text",
            "address" => $this->getValidAddress()
        ]);
    }

    public function testOrderCardWithFile() {
        $expectedRequest = [
            'test' => '1',
            'order[cards_attributes][0][product_name]' => 'standard',
            'order[cards_attributes][0][picture_id]' => '351',
            'order[cards_attributes][0][text]' => 'text',
            'order[cards_attributes][0][company]' => '',
            'order[cards_attributes][0][title]' => '',
            'order[cards_attributes][0][name]' => 'Frany Test',
            'order[cards_attributes][0][street]' => 'Musterstr. 2',
            'order[cards_attributes][0][zip]' => '22345',
            'order[cards_attributes][0][city]' => 'Hamburg',
            'order[cards_attributes][0][state]' => '',
            'order[cards_attributes][0][country]' => 'DE'
        ];
        $expectedPictureRequest = [
            'picture[file]' => file_get_contents(TestHelper::$picturePath)
        ];
        $client = TestHelper::clientWithMockResponses([
            (new GuzzleMockResponse("/apis/reseller/v1/orders"))
                ->withMethod("post")
                ->assertMultipartRequest($expectedRequest),
            (new GuzzleMockResponse("/apis/reseller/v1/pictures"))
                ->withMethod("post")
                ->withBody(TestHelper::$pictureUploadResponse)
                ->assertMultipartRequest($expectedPictureRequest),
        ]);
        $client->orderCard([
            "format" => Postcard::StandardLandscape,
            "picture" => TestHelper::$picturePath,
            "greetingText" => "text",
            "address" => $this->getValidAddress()
        ]);
    }

    public function testOrderPdfCardWithFile() {
        $expectedRequest = [
            'order[cards_attributes][0][pdf]' => file_get_contents($this->getPdfPath()),
            'test' => '1',
            'order[cards_attributes][0][product_name]' => 'standard',
            'order[cards_attributes][0][company]' => '',
            'order[cards_attributes][0][title]' => '',
            'order[cards_attributes][0][name]' => 'Frany Test',
            'order[cards_attributes][0][street]' => 'Musterstr. 2',
            'order[cards_attributes][0][zip]' => '22345',
            'order[cards_attributes][0][city]' => 'Hamburg',
            'order[cards_attributes][0][state]' => '',
            'order[cards_attributes][0][country]' => 'DE'
        ];
        $client = TestHelper::clientWithMockResponses([
            (new GuzzleMockResponse("/apis/reseller/v1/orders"))
                ->withMethod("post")
                ->assertMultipartRequest($expectedRequest)
        ]);
        $client->orderPdfCard([
            "format" => Postcard::StandardLandscape,
            "pdf" => $this->getPdfPath(),
            "address" => $this->getValidAddress()
        ]);
    }

    private function getPdfPath() {
        return dirname(__FILE__) . '/../files/prerendered.pdf';
    }

    private function getValidAddress() {
        return new Address([
            "name" => "Frany Test",
            "street" => "Musterstr. 2",
            "zip" => "22345",
            "city" => "Hamburg",
            "country" => "DE"
        ]);
    }
}
